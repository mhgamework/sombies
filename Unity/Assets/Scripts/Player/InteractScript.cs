﻿using UnityEngine;
using System.Collections;

public class InteractScript : MonoBehaviour
{
    public Camera MyCamera;
    public float MaxInteractionRange;

    // Use this for initialization
    void Start()
    {
        if (MaxInteractionRange == 0)
            MaxInteractionRange = 10;
    }

    // Update is called once per frame
    void Update()
    {
        trySelect();

        if (Input.GetKey(KeyCode.E))
            tryPickup();
    }

    private void trySelect()
    {
        var ray = MyCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit)) return;
        if (hit.distance > MaxInteractionRange) return;

        var c = (ISelectable)hit.collider.GetComponentInParent(typeof(ISelectable));
        if (c == null) return;
        
        c.OnSelected();
    }

    private void tryPickup()
    {
        var ray = MyCamera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit)) return;
        if (hit.distance > MaxInteractionRange) return;
        
        var c = (Interactable)hit.collider.GetComponentInParent(typeof(Interactable));
        if (c == null) return;

        //Debug.Log(c);

        c.OnInteract();



    }
}
