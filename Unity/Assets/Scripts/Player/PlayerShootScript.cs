﻿using UnityEngine;
using System.Collections;

public class PlayerShootScript : MonoBehaviour
{
    public Camera Camera;
    public GameObject BulletPrefab;
    public float BulletSpeed = 10;
    public float BulletOffset = 0.5f;

    public int AmmoLeft = 25;
    public int AmmoMax = 25;

    public float Health = 1;

    private float waitForRegen = 0;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (waitForRegen < 0.001f)
            Health = Mathf.Min(Health + 5*Time.deltaTime, 1);
        else
            waitForRegen -= Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.F))
            tryShoot();
    }

    private void tryShoot()
    {
        if (AmmoLeft <= 0) return;
        AmmoLeft--;

        var newBullet = (GameObject) Instantiate(BulletPrefab);

        var pos = Camera.transform.position;
        var dir = Camera.transform.forward;

        newBullet.transform.position = pos + BulletOffset * dir;
        newBullet.transform.rotation = Camera.transform.rotation;
        newBullet.GetComponent<Rigidbody>().velocity = dir * BulletSpeed;
    }

    public void Damage(float f)
    {
        Health -= f;
        waitForRegen = 5;
        if (Health < 0)
        {
            // DIE!
            Health = 0;
        }
        
    }
}
