﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        Destroy(this.gameObject);

        if (other.gameObject.GetComponentInParent<SombieBrain>() != null)
        {
            ((SombieBrain)(other.gameObject.GetComponentInParent<SombieBrain>())).Die();
        }
    }
}
