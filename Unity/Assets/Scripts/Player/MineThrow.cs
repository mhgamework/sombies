﻿using UnityEngine;
using System.Collections;

public class MineThrow : MonoBehaviour
{
    public Camera MyCamera;

    public int MineAmmoLeft = 5;
    public GameObject Mine;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.G))
            throwMine();
	}

    private void throwMine()
    {
        if (MineAmmoLeft <= 0) return;

        MineAmmoLeft--;
        var mine = Instantiate(Mine, MyCamera.transform.position + new Vector3(0, 1, 0)*0.5f + MyCamera.transform.forward*2, MyCamera.transform.rotation);
        
    }
}
