﻿using UnityEngine;
using System.Collections.Generic;

public class CaveComponentInteractable : MonoBehaviour, Interactable
{
    public Camera PlayerCamera;
    public int NumberOfClicksToDestroy;
    public float DestroyTimeOut;
    public List<GameObject> CaveBlockStages;

    private float currentTimeOut;
    private int nbTimesClicked;

    // Use this for initialization
    void Start()
    {
        if (NumberOfClicksToDestroy == 0)
            NumberOfClicksToDestroy = 10;
        if (DestroyTimeOut == 0)
            DestroyTimeOut = 1f;

        nbTimesClicked = Random.Range(0, NumberOfClicksToDestroy - 1);

        currentTimeOut = 0; //give feedback as soon as player interacts

        updateMesh();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Desintegrate()
    {
        Destroy(gameObject);
    }

    //Supposes that key is held down
    public void OnInteract()
    {

        currentTimeOut -= Time.deltaTime;
        if (currentTimeOut < 0)
        {
            currentTimeOut = DestroyTimeOut;
        }
        else return;

        particleSystem.Play();
        nbTimesClicked++;

        if (nbTimesClicked < NumberOfClicksToDestroy)
            updateMesh();
        else
            Desintegrate();

    }

    private void updateMesh()
    {
        var nbStages = CaveBlockStages.Count;

        if (nbStages == 0)
            return;

        var indexToShow = Mathf.RoundToInt((float)nbTimesClicked / (float)NumberOfClicksToDestroy * nbStages);
        for (int i = 0; i < CaveBlockStages.Count; i++)
        {
            CaveBlockStages[i].gameObject.renderer.enabled = i == indexToShow;
        }
    }

}
