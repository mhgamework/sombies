﻿using UnityEngine;
using System.Collections;

public class ItemInteractable : MonoBehaviour, Interactable {
    public int AmmoAmount = 5;

    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnInteract()
    {
        var playerShootScript = GameObject.Find("Bob").GetComponent<PlayerShootScript>();
        AmmoAmount = 5;
        playerShootScript.AmmoLeft = Mathf.Min(playerShootScript.AmmoLeft + AmmoAmount, playerShootScript.AmmoMax);
        Destroy(this.gameObject);
    }
}
