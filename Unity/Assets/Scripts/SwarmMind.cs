﻿using DirectX11;
using MHGameWork.TheWizards.SkyMerchant._Engine.DataStructures;
using UnityEngine;
using System.Collections;
using System.Linq;

public class SwarmMind : MonoBehaviour
{
    public float CellSize = 5;
    public int MaxSombies = 50;
    public Array2D<float> heat;
    //public Array2D<bool> walls;
    public int FieldSize = 20;
    private CaveManager caveSpawner;

    private int sombieCount;

    public int SombieCount
    {
        get { return sombieCount; }
    }

    // Use this for initialization
    void Start()
    {
        heat = new Array2D<float>(new Point2(FieldSize, FieldSize));
        caveSpawner = Object.FindObjectOfType(typeof(CaveManager)) as CaveManager;
        /*walls = new Array2D<bool>(new Point2(FieldSize, FieldSize));
        walls[new Point2(5, 6)] = true;
        walls[new Point2(4, 6)] = true;
        walls[new Point2(3, 6)] = true;
        walls[new Point2(6, 6)] = true;
        walls[new Point2(7, 6)] = true;*/
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Bob") != null) // Idea use ZOmbieTarget script to set heat
        {
            var bobPos = ToGridPoint(GameObject.Find("Bob").transform.position);
            if (heat.InArray(bobPos))
                heat[bobPos] = 100;
        }
        //heat[new Point2(5, 5)] = 100;
        heat.ForEach((val, p) =>
            {
                if (isWall(p))
                {
                    heat[p] = 0;
                    return;
                }
                heat[p] = heat.Get4Connected(p).Average();
            });
    }

    public void AddSombie(SombieBrain brain)
    {
        sombieCount++;
    }
    public void RemoveSombie(SombieBrain brain)
    {
        sombieCount--;
    }

    private bool isWall(Point2 p)
    {
        if (caveSpawner == null) return false;
        return caveSpawner.hasWallAtGridCoord(p);
    }

    void OnDrawGizmos()
    {

        if (heat == null) return;
        heat.ForEach((val, p) =>
            {
                if (val < 0.01f) return;
                Gizmos.DrawCube(transform.position + new Vector3(1, 0, 1) * CellSize / 2 + new Vector3(p.X, 0, p.Y) * CellSize + Vector3.up * val * 0.5f, new Vector3(2, val, 2));

            });
        Gizmos.color = Color.yellow;
    }

    public Vector3 GetBetterSquareCenter(Vector3 position)
    {
        if (heat == null) return position;

        var pos = ToGridPoint(position);
        var better = heat.Get4ConnectedPositions(pos).OrderByDescending(p => heat[p]).First();
        if (heat[pos] >= heat[better]) return position;
        return (better.ToVector3(0) + new Vector3(0.5f, 0, 0.5f)) * CellSize;
    }

    public Point2 ToGridPoint(Vector3 worldPoint)
    {
        worldPoint /= CellSize;
        var pos = new Point2((int)Mathf.Floor(worldPoint.x), (int)Mathf.Floor(worldPoint.z));
        return pos;
    }

    public bool IsPlayerSensedAt(Vector3 position)
    {
        return heat[ToGridPoint(position)] > 0;
    }
}
