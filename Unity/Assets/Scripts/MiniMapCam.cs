﻿using UnityEngine;
using System.Collections;

public class MiniMapCam : MonoBehaviour
{
    public GameObject Player;
    public Camera MiniMapCamera;
    public float Height;
    
	// Use this for initialization
	void Start ()
	{
        if(Height == 0)
            Height = 6;
	}
	
	// Update is called once per frame
	void Update ()
	{

	    var playerRot = Player.transform.rotation;

        transform.position = new Vector3(Player.transform.position.x, Height, Player.transform.position.z);
	    transform.rotation = playerRot;
	}
}
