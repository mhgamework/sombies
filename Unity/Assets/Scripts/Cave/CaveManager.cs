﻿using System;
using System.Linq;
using DirectX11;
using MHGameWork.TheWizards.SkyMerchant._Engine.DataStructures;
using UnityEngine;
using System.Collections.Generic;

public class CaveManager : MonoBehaviour
{
    public int SizeX;
    public int SizeZ;
    public float BlockSize;

    public int Seed;

    public int minRoomSize;
    public int maxRoomSize;


    public GameObject SombieSpawner;
    public GameObject CaveComponent;
    public GameObject Sombie;
    public GameObject Item;
    public GameObject Door;


    private int roomDivideLimit;


    private Array2D<GameObject> grid;

    public class RoomBoundary
    {
        public int minX;
        public int maxX;
        public int minZ;
        public int maxZ;
        public bool splittedInXDir;
    }

    public class CellCoordinate
    {
        public int X;
        public int Z;
    }

    // Use this for initialization
    void Start()
    {
        if (SizeX == 0)
            SizeX = 20;
        if (SizeZ == 0)
            SizeZ = 20;
        if (BlockSize == 0)
            BlockSize = 1;
        if (minRoomSize == 0)
            minRoomSize = 2;
        if (maxRoomSize == 0)
            maxRoomSize = 5;

        spawnCave(Seed);
    }

    public bool hasWallAtGridCoord(Point2 coord)
    {
        if (grid == null) return false;
        return grid[coord] != null;
    }


    private void spawnCave(int seed)
    {
        var rnd = new System.Random(seed);
        var maxNbIterations = SizeX * SizeZ;

        //room boundaries
        var boundaries = getAllRoomBoundaries(rnd.Next(), maxNbIterations).ToList();

        //render room boundaries (for testing)
        /*foreach (var room in rooms)
        {
            for (int i = room.minX; i <= room.maxX; i++)
            {
                Instantiate(CaveComponent, new Vector3(i * BlockSize, 0, room.minZ * BlockSize) + transform.position, Quaternion.identity);
                Instantiate(CaveComponent, new Vector3(i * BlockSize, 0, room.maxZ * BlockSize) + transform.position, Quaternion.identity);
            }
            for (int j = room.minZ; j <= room.maxZ; j++)
            {
                Instantiate(CaveComponent, new Vector3(room.minX * BlockSize, 0, j * BlockSize) + transform.position, Quaternion.identity);
                Instantiate(CaveComponent, new Vector3(room.maxX * BlockSize, 0, j * BlockSize) + transform.position, Quaternion.identity);
            }
        }*/

        var allFreeCells = new List<CellCoordinate>();

        //corridors
        var corridorCells = new List<CellCoordinate>();
        for (int i = 0; i < boundaries.Count - 1; i += 2)
        {
            bool isInXDir = false;
            var tmp = tryGetRoomConnection(boundaries[i], boundaries[i + 1], out isInXDir); //todo
            populateCorridor(rnd.Next(), tmp, isInXDir);
            foreach (var cell in tmp)
            {
                addCellIfNotExists(cell, corridorCells, corridorCells);
            }
        }
        allFreeCells.AddRange(corridorCells);


        //rooms
        var roomCells = new List<CellCoordinate>();
        foreach (var boundary in boundaries)
        {
            var tmp = getRoom(rnd.Next(), boundary);
            foreach (var cell in tmp)
            {
                addCellIfNotExists(cell, allFreeCells, roomCells);
            }
        }
        allFreeCells.AddRange(roomCells);

        //room population
        //todo




        // Setup datastorage
        grid = new Array2D<GameObject>(new Point2(SizeX, SizeZ));

        //render
        for (int i = 0; i < SizeX; i++)
        {
            for (int j = 0; j < SizeZ; j++)
            {
                if (!allFreeCells.Exists(e => e.X == i && e.Z == j))
                {
                    if (i < 3 && j < 3) continue;
                    var comp = (GameObject)Instantiate(CaveComponent, new Vector3(i * BlockSize, 0, j * BlockSize) + transform.position, Quaternion.identity);
                    grid[new Point2(i, j)] = comp;
                }
            }
        }


        // Spawn zombie spawners
        for (int i = 0; i < 3; i++)
        {
            var pos = new Point2(rnd.Next(3, SizeX - 3), rnd.Next(3, SizeZ - 3));
            var toRemove = new[] { pos + new Point2(0, 0), pos + new Point2(0, 1), pos + new Point2(1, 0), pos + new Point2(1, 1) };
            foreach (var p in toRemove)
            {
                if (grid[p] != null) Destroy(grid[p]);
                grid[p] = null; // Happens auto
            }

            Instantiate(SombieSpawner, (pos.ToVector3(0) + new Vector3(1, 0, 1)) * BlockSize, Quaternion.identity);
        }

    }

    private void populateCorridor(int seed, List<CellCoordinate> corridorCells, bool isInXDir)
    {
		int corridorOffset = Mathf.FloorToInt (maxRoomSize * 0.5f); //an offset to ensure doors are not spawned inside rooms
        if (corridorCells.Count < 2*corridorOffset)
            return;

        var rnd = new System.Random(seed);
		var cellToSpawnOn = corridorCells[rnd.Next(corridorOffset, corridorCells.Count-corridorOffset)];
        var angle = isInXDir ? 90 : 0;
        var rotation = Quaternion.AngleAxis(angle, Vector3.up);

        Instantiate(Door, new Vector3(cellToSpawnOn.X * BlockSize - 0.5f*BlockSize, 0, cellToSpawnOn.Z * BlockSize + 0.5f*BlockSize) + transform.position, rotation);

    }

    private IEnumerable<CellCoordinate> getRoom(int seed, RoomBoundary boundaries)
    {
        var ret = new List<CellCoordinate>();
        var rnd = new System.Random(seed);

        ret.AddRange(getCenteredCorridor(rnd.Next(), boundaries));
        return growOnCorridor(rnd.Next(), ret, boundaries);
    }

    private List<CellCoordinate> tryGetRoomConnection(RoomBoundary roomA, RoomBoundary roomB, out bool isInXDir)
    {
        isInXDir = false;
        var ret = new List<CellCoordinate>();

        var centerAx = Mathf.RoundToInt((roomA.maxX - roomA.minX) * 0.5f + roomA.minX);
        var centerAz = Mathf.RoundToInt((roomA.maxZ - roomA.minZ) * 0.5f + roomA.minZ);
        var centerBx = Mathf.RoundToInt((roomB.maxX - roomB.minX) * 0.5f + roomB.minX);
        var centerBz = Mathf.RoundToInt((roomB.maxZ - roomB.minZ) * 0.5f + roomB.minZ);

        if (centerAx > roomB.minX && centerAx < roomB.maxX)
        {
            isInXDir = false;
            var inc = centerAz < centerBz ? 1 : -1;
            for (int i = centerAz; i != centerBz; i += inc)
            {
                ret.Add(new CellCoordinate { X = centerAx, Z = i });
            }
        }
        else if (centerAz > roomB.minZ && centerAz < roomB.maxZ)
        {
            isInXDir = true;
            var inc = centerAx < centerBx ? 1 : -1;
            for (int i = centerAx; i != centerBx; i += inc)
            {
                ret.Add(new CellCoordinate { X = i, Z = centerAz });
            }
        }

        return ret;
    }

    private IEnumerable<CellCoordinate> getCenteredCorridor(int seed, RoomBoundary boundaries)
    {
        var ret = new List<CellCoordinate>();

        int from;
        int to;
        int mid;
        int otherMid;
        var rnd = new System.Random(seed);

        if (!boundaries.splittedInXDir)
        {
            mid = Mathf.RoundToInt((boundaries.maxX - boundaries.minX) * 0.5f + boundaries.minX);
            otherMid = Mathf.RoundToInt((boundaries.maxZ - boundaries.minZ) * 0.5f + boundaries.minZ);
            from = rnd.Next(boundaries.minX, mid);


            to = rnd.Next(mid, boundaries.maxX);
            for (int i = from; i <= to; i++)
            {
                ret.Add(new CellCoordinate { X = i, Z = otherMid });
            }
        }
        else
        {
            mid = Mathf.RoundToInt((boundaries.maxZ - boundaries.minZ) * 0.5f + boundaries.minZ);
            otherMid = Mathf.RoundToInt((boundaries.maxX - boundaries.minX) * 0.5f + boundaries.minX);
            from = rnd.Next(boundaries.minZ, mid);
            to = rnd.Next(mid, boundaries.maxZ);
            for (int i = from; i <= to; i++)
            {
                ret.Add(new CellCoordinate { X = otherMid, Z = i });
            }
        }

        return ret;
    }

    private IEnumerable<CellCoordinate> growOnCorridor(int seed, List<CellCoordinate> corridorCells, RoomBoundary boundaries)
    {
        var ret = corridorCells.ToList();

        var rnd = new System.Random(seed);
        var nbGrowStepsRange = boundaries.splittedInXDir
                                   ? Mathf.RoundToInt(0.5f * (boundaries.maxZ - boundaries.minZ))
                                   : Mathf.RoundToInt(0.5f * (boundaries.maxX - boundaries.minX));
        var nbGrowSteps = rnd.Next(0, nbGrowStepsRange);

        var newCells = corridorCells;
        for (int i = 0; i < nbGrowSteps; i++)
        {
            var tmp = new List<CellCoordinate>();
            foreach (var cell in newCells)
            {
                if (rnd.Next(0, 10) < 5)
                    continue;

                addCellIfNotExists(new CellCoordinate { X = cell.X + 1, Z = cell.Z }, ret, tmp);
                addCellIfNotExists(new CellCoordinate { X = cell.X - 1, Z = cell.Z }, ret, tmp);
                addCellIfNotExists(new CellCoordinate { X = cell.X, Z = cell.Z + 1 }, ret, tmp);
                addCellIfNotExists(new CellCoordinate { X = cell.X, Z = cell.Z - 1 }, ret, tmp);
                ret.AddRange(tmp);
            }
            newCells = tmp.ToList();
        }

        return ret;
    }

    private void addCellIfNotExists(CellCoordinate cell, List<CellCoordinate> toCheck, List<CellCoordinate> toAdd)
    {
        if (!toCheck.Exists(e => e.X == cell.X && e.Z == cell.Z))
            toAdd.Add(cell);
    }

    private IEnumerable<RoomBoundary> getAllRoomBoundaries(int seed, int maxIterations)
    {
        var ret = new List<RoomBoundary>();
        var tempRooms = new List<RoomBoundary>
            {
                new RoomBoundary {minX = 0, maxX = SizeX, minZ = 0, maxZ = SizeZ, splittedInXDir = false}
            };
        var rnd = new System.Random(seed);

        for (int i = 0; i < maxIterations; i++)
        {
            if (!calculateRoomBoundaries_internal(rnd.Next(), tempRooms, out ret))
                return ret;

            tempRooms = ret.ToList();
        }

        return ret;
    }

    //return whether any new rooms are created
    private bool calculateRoomBoundaries_internal(int seed, List<RoomBoundary> startRooms, out List<RoomBoundary> endRooms)
    {
        var ret = false;
        endRooms = new List<RoomBoundary>();

        var rnd = new System.Random(seed);

        foreach (var room in startRooms)
        {
            RoomBoundary r01;
            RoomBoundary r02;
            if (split(rnd.Next(), room, out r01, out r02))
            {
                ret = true;
                endRooms.Add(r01);
                endRooms.Add(r02);
            }
            else
                endRooms.Add(room);
        }

        return ret;
    }

    private bool split(int seed, RoomBoundary room, out RoomBoundary r01, out RoomBoundary r02)
    {
        r01 = null;
        r02 = null;

        var xDiff = room.maxX - room.minX;
        var zDiff = room.maxZ - room.minZ;

        var splitInXDir = !room.splittedInXDir;

        if (splitInXDir && xDiff < maxRoomSize + minRoomSize + 1) return false;
        if (!splitInXDir && zDiff < maxRoomSize + minRoomSize + 1) return false;

        var rnd = new System.Random(seed);

        if (splitInXDir)
        {
            var splitX = rnd.Next(room.minX, room.maxX);
            r01 = new RoomBoundary { minX = room.minX, maxX = splitX, minZ = room.minZ, maxZ = room.maxZ, splittedInXDir = splitInXDir };
            r02 = new RoomBoundary { minX = splitX + 1, maxX = room.maxX, minZ = room.minZ, maxZ = room.maxZ, splittedInXDir = splitInXDir };
            return true;
        }

        var splitZ = rnd.Next(room.minZ, room.maxZ);
        r01 = new RoomBoundary { minX = room.minX, maxX = room.maxX, minZ = room.minZ, maxZ = splitZ, splittedInXDir = splitInXDir };
        r02 = new RoomBoundary { minX = room.minX, maxX = room.maxX, minZ = splitZ, maxZ = room.maxZ, splittedInXDir = splitInXDir };
        return true;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
