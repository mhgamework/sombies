﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{
    public GameObject AmmoText;
    public GameObject DamageIndicator;
    private GameObject bob;
    private PlayerShootScript playerShootScript;
    private SwarmMind mind;
    // Use this for initialization
    void Start()
    {
        bob = GameObject.Find("Bob");
        playerShootScript = bob.GetComponent<PlayerShootScript>();
        mind = FindObjectOfType(typeof (SwarmMind)) as SwarmMind;
    }

    // Update is called once per frame
    void Update()
    {
        AmmoText.GetComponent<Text>().text = string.Format("Ammo: {0}/{1}", playerShootScript.AmmoLeft,
                                                           playerShootScript.AmmoMax)
                                                           + "\n Sombies: " + mind.SombieCount;

        var c = DamageIndicator.GetComponent<RawImage>().color;
        c.a = 1 - playerShootScript.Health;
        DamageIndicator.GetComponent<RawImage>().color = c;
    }
}
