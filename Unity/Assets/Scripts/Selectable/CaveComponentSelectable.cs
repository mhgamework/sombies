﻿using UnityEngine;
using System.Collections;

public class CaveComponentSelectable : MonoBehaviour, ISelectable
{

    public GameObject HighlightObject;

    private bool isSelected;

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        HighlightObject.renderer.enabled = isSelected;
        if (isSelected)
            isSelected = false;
    }

    public void OnSelected()
    {
        isSelected = true;
    }
}
