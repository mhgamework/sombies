﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SombieSpawner : MonoBehaviour
{
    public GameObject Sombie;
    public int PackSize = 10;
    public float SpawnInterval = 10;
    private SwarmMind mind;

    // Use this for initialization
    void Start()
    {
        mind = (SwarmMind) FindObjectOfType(typeof (SwarmMind));
        StartCoroutine(spawn().GetEnumerator());
    }

    private IEnumerable<YieldInstruction> spawn()
    {
        while (true)
        {
            if (mind == null) yield break;
            if (mind.SombieCount > mind.MaxSombies)
            {
                yield return new WaitForSeconds(2);
                continue;
            }
            if (!mind.IsPlayerSensedAt(transform.position))
            {
                yield return new WaitForSeconds(2);
                continue;
            }

            for (int i = 0; i < PackSize; i++)
            {
                SpawnSombie();
            }
            yield return new WaitForSeconds(SpawnInterval);
        }


    }

    public void SpawnSombie()
    {
        var dir = Random.onUnitSphere;
        dir.y = 0;
        dir.Normalize();

        Instantiate(Sombie, transform.position + dir * 2, Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
