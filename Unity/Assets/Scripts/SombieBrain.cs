﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Linq;

public class SombieBrain : MonoBehaviour
{
    private Vector3 destination;
    public float WalkSpeed = 0.5f;
    public float HitDamage;

    private IEnumerator<YieldInstruction> thinker;
    private SwarmMind mind;
    private GameObject player;

    void Awake()
    {
        player = findPlayer();
        mind = (SwarmMind)FindObjectOfType(typeof(SwarmMind));
        setDestination(transform.position);
        thinker = Think().GetEnumerator();
        StartCoroutine(thinker);
        GetComponent<CharacterController>().enabled = false;
        //Destroy(transform.FindChild("Trigger").gameObject);
        //Destroy(transform.FindChild("Model").gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        var controller = GetComponent<CharacterController>();
        var dir = Vector3.Normalize(destination - transform.position);

        if (controller != null && controller.enabled)
        {
            controller.SimpleMove(dir * WalkSpeed);
        }
        else
        {
            transform.position += dir * WalkSpeed * Time.deltaTime;
        }
        //Debug.Log(destination);

        if (player != null)
            transform.rotation = Quaternion.LookRotation(player.transform.position - transform.position);

    }

    void OnEnable()
    {
        if (mind != null) mind.AddSombie(this);
    }
    void OnDisable()
    {
        if (mind != null) mind.RemoveSombie(this);

    }

    public IEnumerable<YieldInstruction> Think()
    {
        while (true)
        {
            //Debug.Log("UUUUUUUUhh");

            FallDoorScript door = null;

            HitDamage = 0.1f;
            if (tryHitPlayer()) ;
            else if (tryHitDoor()) ;
            else setBetterDestination();

            
            //yield return new WaitForSeconds(Random.Range(0.5f, 1f));
            yield return new WaitForSeconds(Random.Range(1f, 3f));
        }
    }

    private void setBetterDestination()
    {
        if (GameObject.Find("SwarmMind") == null)
        {
            setDestination(transform.position);
            return;
        }
        // Follow heatmap
        var swarmMind = GameObject.Find("SwarmMind").GetComponent<SwarmMind>();
        setDestination(swarmMind.GetBetterSquareCenter(transform.position));
    }

    private bool tryHitDoor()
    {
        var door = getDoorInRange();
        if (door == null) return false;
        Debug.Log("DOOOOORR");
        door.DamageDoor(HitDamage);
        return true;
    }

    private bool tryHitPlayer()
    {
        var player = findPlayer();
        if (player == null) return false;
        if (!(Vector3.Distance(player.transform.position, transform.position) < 2f)) return false;
        // Hit!!
        player.GetComponent<PlayerShootScript>().Damage(HitDamage);
        Debug.Log("Njam");
        return true;
    }

    private FallDoorScript getDoorInRange()
    {
        var objects = FindObjectsOfType(typeof(FallDoorScript));
        Object first = null;
        foreach (Object o in objects)
        {
            Transform doorChild = ((Component)o).transform.FindChild("Door");
            Debug.Log("See door!");
            if (doorChild == null) continue;
            float distance = Vector3.Distance(doorChild.collider.bounds.center, transform.position);
            Debug.Log(distance);
            if (distance < 4)
            {
                first = o;
                break;
            }
        }
        return (FallDoorScript)first;

    }

    private void setDestination(Vector3 position)
    {
        destination = position;
        //GetComponent<AstarAI>().targetPosition = position;
    }

    public void Die()
    {
        StopCoroutine(thinker);
        StartCoroutine(onDie().GetEnumerator());
    }

    private IEnumerable<YieldInstruction> onDie()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            Destroy(child.gameObject);
        }

        var cc = GetComponent(typeof(CharacterController)) as CharacterController;
        cc.enabled = false;

        gameObject.particleSystem.Play();

        yield return new WaitForSeconds(2);

        Destroy(gameObject);
    }

    private GameObject findPlayer()
    {
        if (player != null) return player;
        player = GameObject.Find("Bob");
        return player;
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(destination, 1);
    }
}
