﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class FallDoorScript : MonoBehaviour, Interactable
{
    public bool InitialOpen = true;
    public float TimeToFall = 3;
    public float HealthPerBar = 0.2f;

    private Transform doorPart;

    private bool closed;
    private Transform destructables;
    private float currentBarHealth = 0f;

    // Use this for initialization
    void Start()
    {
        destructables = transform.FindChild("Door/Destructables");

        closed = !InitialOpen;
        doorPart = transform.FindChild("Door");
        setDoorState(InitialOpen ? 1 : 0);
        currentBarHealth = HealthPerBar;

    }

    private void destroyBar()
    {
        if (destructables.childCount == 0) return;
        var bar = destructables.GetChild(0);
        Destroy(bar.gameObject);
        currentBarHealth = HealthPerBar;
        if (getBarCount() == 1) // Only gets removed at end of frame
            destroyDoor();
    }

    private void destroyDoor()
    {
        Destroy(doorPart.gameObject);
        doorPart = null;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void DamageDoor(float amount)
    {
        while (amount > 0 && getBarCount() > 0)
        {
            if (currentBarHealth > amount)
            {
                currentBarHealth -= amount;
                return;
            }
            amount -= HealthPerBar;
            destroyBar();
        }
    }

    private int getBarCount()
    {
        if (doorPart == null) return 0;
        return destructables.transform.childCount;
    }

    public void FallToClose()
    {
        if (closed) return;
        closed = true;
        StartCoroutine(fallDown().GetEnumerator());
    }
    private IEnumerable<YieldInstruction> fallDown()
    {
        for (float i = 0; i < TimeToFall; i += Time.deltaTime)
        {
            float downAmount = (TimeToFall - i) / TimeToFall;
            setDoorState(downAmount);
            yield return new WaitForEndOfFrame();
        }
    }

    private void setDoorState(float upAmount)
    {
        doorPart.localPosition = new Vector3(0, upAmount * 5, 0);
    }

    public void OnInteract()
    {
        DamageDoor(Time.deltaTime * 0.3f);
    }
}
