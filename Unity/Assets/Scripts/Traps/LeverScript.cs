﻿using UnityEngine;
using System.Collections;

public class LeverScript : MonoBehaviour, Interactable
{

    public GameObject Target;
    public float State = 1;

    private bool moving;
    private Transform turnPart;

    // Use this for initialization
    void Start()
    {
        turnPart = transform.FindChild("TurnPart");
        set(State);
    }

    private void set(float rotation)
    {
        turnPart.localRotation = Quaternion.Euler(Mathf.Lerp(-45, 45, rotation), 0, 0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnInteract()
    {
        if (moving) return;

        StartCoroutine(toggle().GetEnumerator());

    }

    private IEnumerable toggle()
    {
        moving = true;

        float goal = State < 0.5 ? 1 : 0;
        float speed = 1;

        float dir = Mathf.Sign(goal - State);
        while (State != goal)
        {
            State = Mathf.Clamp(State + speed * dir * Time.deltaTime, 0, 1);
            set(State);
            yield return new WaitForEndOfFrame();
        }
        invokeTarget();

        moving = false;
    }

    private void invokeTarget()
    {
        if (Target.GetComponent<FallDoorScript>() != null)
        {
            var s = Target.GetComponent<FallDoorScript>();
            s.FallToClose();
        }
    }
}
