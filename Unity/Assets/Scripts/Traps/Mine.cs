﻿using UnityEngine;
using System.Collections;

public class Mine : MonoBehaviour
{
    public float DetonationRadius;
    public float DetonationTime;
    private float lifeTime;

    public GameObject RadiusModel;

    private bool exploded;
    private float explodedTimeout;

    // Use this for initialization
    void Start()
    {
        if (DetonationTime == 0)
            DetonationTime = 5;

        RadiusModel.transform.localScale = RadiusModel.transform.localScale * DetonationRadius;

        explodedTimeout = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!exploded)
        {
            lifeTime += Time.deltaTime;
            if (lifeTime > DetonationTime)
                explode();
            return;
        }


        explodedTimeout -= Time.deltaTime;
        if (explodedTimeout < 0)
            Destroy(gameObject);
    }


    private void explode()
    {
        var sombies = FindObjectsOfType(typeof(SombieBrain)) as SombieBrain[];
        foreach (SombieBrain sombie in sombies)
        {
            if (Vector3.Distance(transform.position, sombie.transform.position) < DetonationRadius)
                sombie.Die();
        }

        var caveComps = FindObjectsOfType(typeof(CaveComponentInteractable)) as CaveComponentInteractable[];
        foreach (CaveComponentInteractable cc in caveComps)
        {
            if (Vector3.Distance(transform.position, cc.transform.position) < DetonationRadius)
                cc.Desintegrate();
        }



        Destroy(RadiusModel);
        gameObject.renderer.enabled = false;
        exploded = true;
        gameObject.particleSystem.Play();
    }
}
